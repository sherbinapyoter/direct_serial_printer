String int_data = "";
int delimiter = (int) '\n';
byte intBuffer[12];

int pins_status[54] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // 0 - не инециализирован, 1 - read, 2 - write
int pins_write_status[54] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // 0 - не имеет статус write, 1 - false, 2 - true

void setup() {
  Serial.begin(9600);
  Serial.println("!start");
}

void loop() {
  int_data = "";
  // Serial.println("!loop");
  while (true) {
    if (Serial.available()) {
      int ch = Serial.read();
      // if (ch == -1) {
      //   // Handle error
      // } else 
      if (ch == delimiter) {
        break;
      } else {
        int_data += (char) ch;
      }
    }
  }
  // Serial.println("!readAll");
  if (int_data != "") {
    //Serial.println(int_data);
  } else {
    return;
  }
  char letter = '\n';
  long number = 0;
  String added_letters = "";

  letter = int_data[0];

  if (letter == 'v') {
    added_letters += int_data[1];
    added_letters += int_data[2];
    added_letters += int_data[3];
  } else if (letter == 'u') {
    added_letters += int_data[1];
    added_letters += int_data[2];
  } else if (int_data.length() > 1) {
    String nbr = "";
    for (int i = 1; i != int_data.length(); i++) {
      nbr += int_data[i];
    }
    int intLength = nbr.length() + 1;
    nbr.toCharArray(intBuffer, intLength);
    number = atol(intBuffer);
  }

  if (letter == 'r') {
    pins_status[number] = 1;
    pinMode(number, INPUT);
  } else if (letter == 'w') {
    pins_status[number] = 2;
    pins_write_status[number] = 1;
    pinMode(number, OUTPUT);
  } else if (letter == 't' && pins_status[number] == 2) {
    pins_status[number] = 2;
    pins_write_status[number] = 2;
    digitalWrite(number, 1);
  } else if (letter == 'f' && pins_status[number] == 2) {
    pins_status[number] = 2;
    pins_write_status[number] = 1;
    digitalWrite(number, 0);
  } else if (letter == 's') {
    //Serial.println(number);
    delay(number/1000);
    delayMicroseconds(number%1000);
    //Serial.println("!sleepover");
  } else if (letter == 'v' && added_letters == "ral") {
    String aw = "aw:";
    for (int i = 0; i != 54; i++) {
      if (pins_status[i] == 1){
        aw += i;
        aw += ':';
        aw += digitalRead(i);
        aw += ';';
      }
    }
    aw += "A0:";
    aw += analogRead(A0);
    aw += ";A1:";
    aw += analogRead(A1);
    aw += ";A2:";
    aw += analogRead(A2);
    aw += ";A3:";
    aw += analogRead(A3);
    aw += ";A4:";
    aw += analogRead(A4);
    aw += ";A5:";
    aw += analogRead(A5);
    aw += ";A6:";
    aw += analogRead(A6);
    aw += ";A7:";
    aw += analogRead(A7);
    aw += ";A8:";
    aw += analogRead(A8);
    aw += ";A9:";
    aw += analogRead(A9);
    aw += ";A10:";
    aw += analogRead(A10);
    aw += ";A11:";
    aw += analogRead(A11);
    aw += ";A12:";
    aw += analogRead(A12);
    aw += ";A13:";
    aw += analogRead(A13);
    aw += ";A14:";
    aw += analogRead(A14);
    aw += ";A15:";
    aw += analogRead(A15);
    
    Serial.println(aw);
  } else if (letter == 'v' && added_letters == "ips") {
    String init = "init:";
    for (int i = 0; i != 54; i++) {
      init += pins_status[i];
      init += ';';
    }
    Serial.println(init);
  } else if (letter == 'v' && added_letters == "wal") {
    String init = "wal:";
    for (int i = 0; i != 54; i++) {
      if (pins_status[i] == 2){
        init += i;
        init += ':';
        if (pins_write_status[i] == 1) {
          init += 'f';
        } else {
          init += 't';
        }
        init += ';';
      }
    }
    Serial.println(init);
  }
}
